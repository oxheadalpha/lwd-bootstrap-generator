open! Base

let base_bootstrap ~page_title ?loading_gif ~extra_js ?bootstrap_css () =
  (* From there: https://getbootstrap.com/docs/4.5/getting-started/introduction/ *)
  let loading_uri =
    Option.map loading_gif ~f:(fun path ->
        let content_type = "image/gif" in
        let data = Stdio.In_channel.read_all path in
        Fmt.str "data:%s;base64,%s" content_type
          (Base64.encode_exn ~pad:true ~alphabet:Base64.default_alphabet data))
  in
  let script s =
    Fmt.str {|<script type="text/javascript">
//<![CDATA[
%s
//]]>
</script>|} s
  in
  String.concat ~sep:""
    [
      {html|
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->|html};
      (match bootstrap_css with
      | None -> Fmt.str "<style>%s</style>" Data.bootstrap_min_css
      (* Fmt.str
         {html|<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">|html} *)
      | Some uri -> Fmt.str {html|<link rel="stylesheet" href=%S >|html} uri);
      {html|<title>|html};
      page_title;
      {html|</title>
  </head>
  <body>
    <div id="attach-ui">
|html};
      Fmt.str "<h2>Loading %s …</h2>" page_title;
      begin
        match loading_uri with
        | Some u -> Fmt.str "<img src=%S/><br/><br/>" u
        | None ->
            {|<div class="spinner-border" role="status"><span class="sr-only">Loading</span></div>|}
      end;
      "</div>";
      script Data.jquery_min_js;
      script Data.popper_min_js;
      script Data.bootstrap_min_js;
      (* {html|<script src="main-client.js"></script>|html}; *)
      String.concat ~sep:"\n"
        (List.map extra_js ~f:(fun path ->
             let data = Stdio.In_channel.read_all path in
             script data));
      "<script>";
      List.map [ "shown"; "show"; "hide"; "hidden" ] ~f:(fun evname ->
          Fmt.str
            {html|
$(document).on('%s.bs.collapse', function (e) {
   console.log('##%s ' + e.type + ' TRGT ' + e.target.id);
   var ev = new CustomEvent('collapse-%s', { detail: e.target.id });
   document.body.dispatchEvent(ev);
})|html}
            evname evname evname)
      |> String.concat ~sep:"\n";
      (*
$(document).on('hidden.bs.collapse', function (e) {
   console.log('##shown ' + e.type + ' TRGT ' + e.target.id);
   var ev = new CustomEvent('collapse-hidden', { detail: e.target.id } );
   document.body.dispatchEvent(ev);
})
 *)
      {html|
</script>
  </body>
</html>
|html};
    ]

let main () =
  let open Stdlib.Arg in
  let loading_gif = ref "" in
  let title = ref "NO-TITLE" in
  let bootstrap_css = ref "" in
  let extra_js = ref [] in
  let spec =
    [
      ("--loading", Set_string loading_gif, "<path> A loading.gif to add.");
      ("--title", Set_string title, "<str> The HTML title.");
      ( "--bootstrap-css",
        Set_string bootstrap_css,
        "<uri> An URI to a bootstrap theme." );
      ( "--script",
        String (fun s -> extra_js := s :: !extra_js),
        "<path> Include Javascript file" );
    ]
  in
  let cmd = ref [] in
  parse spec
    (fun s -> cmd := s :: !cmd)
    (Fmt.str
       "usage: %s index '<page-title>' '<loading-gif-location>' [<css-uri>]\n%!"
       Stdlib.Sys.argv.(0));
  match !cmd with
  | [ "index" ] ->
      let non_empty r = match !r with "" -> None | s -> Some s in
      Fmt.pr "%s\n%!"
        (base_bootstrap ~page_title:!title ?loading_gif:(non_empty loading_gif)
           ~extra_js:(List.rev !extra_js)
           ?bootstrap_css:(non_empty bootstrap_css) ())
  | other ->
      Fmt.epr "Unknown command: %a!\n%!" Fmt.Dump.(list string) other;
      Stdlib.exit 2

let () = main ()
