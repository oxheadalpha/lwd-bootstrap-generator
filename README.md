# lwd-bootstrap-generator

> _Generator for HTML files using lwd-bootstrap_

Application for generating `index.html` files with your build system.

## Install

```sh
opam exec -- opam install --with-test --with-doc lwd-bootstrap-generator.opam
```

